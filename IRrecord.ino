/*
 * IRremote: IRsendDemo - demonstrates sending IR codes with IRsend
 * An IR LED must be connected to Arduino PWM pin 3.
 * Version 0.1 July, 2009indexParam
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 */

#include <IRremote.h>

IRsend irsend;//PIN 3
IRrecv irrecv(5);
decode_results results;
int codeType;
int codeBits;
int indexParam;
int indexParamAnt;
int contParam;
int i;
int indexPipe;
int indexPipeAnt;
unsigned int rawCodes[RAWBUF];

String stringRead;
String params;
String comando;

void setup()
{
  Serial.begin(9600);
  stringRead = "";
  irrecv.enableIRIn(); // Start the receiver
}

String obterParam(int ordem){
  indexParam = -2;
  indexParamAnt = indexParam + 1;
  contParam = 1;
  while (indexParam != -1) {
    if(contParam == 1){
      indexParam = -1;
    }
    indexParam = params.indexOf(",",indexParam + 1);
    if(contParam == ordem){
      if(indexParam == -1){
        return params.substring(indexParamAnt + 1);
      }else{
        return params.substring(indexParamAnt + 1, indexParam);
      }
    }
    contParam++;
    indexParamAnt = indexParam;
  }
  return "";
}

void carregarRawCodes(){
  indexPipe = -2;
  indexPipeAnt = indexPipe + 1;
  i = 0;
  while(indexPipe != -1){
    if(i == 0){
      indexPipe = -1;
    }
    indexPipe = comando.indexOf("|", indexPipe + 1);
    
    if(indexPipe == -1){
      rawCodes[i] = comando.substring(indexPipeAnt + 1).toInt();
    }else{
      rawCodes[i] = comando.substring(indexPipeAnt + 1, indexPipe).toInt();
    }
    
    i++;
    indexPipeAnt = indexPipe;
  }
}

void loop() {
  if (Serial.available()) {
    stringRead += (char)Serial.read();
    if(stringRead.startsWith("*") && stringRead.endsWith("#")){
      params = stringRead.substring(stringRead.indexOf(",") + 1,stringRead.indexOf("#"));
      codeType = obterParam(1).toInt();
      codeBits = obterParam(2).toInt();
      comando = stringRead.substring(1,stringRead.indexOf("#"));
      if(codeType == NEC){
        irsend.sendNEC(comando.toInt(), codeBits);
      }else if(codeType == SONY){
        for(int x=0; x < 3; x++){
           irsend.sendSony(comando.toInt(), codeBits);
           delay(40);
        }
        //irsend.sendSony(comando.toInt(), codeBits);
      }else if(codeType == RC5){
        irsend.sendRC5(comando.toInt(), codeBits);
      }else if(codeType == RC6){
        irsend.sendRC6(comando.toInt(), codeBits);
      }else if(codeType == DISH){
        irsend.sendDISH(comando.toInt(), codeBits);
      }else if(codeType == SHARP){
        irsend.sendSharp(comando.toInt(), codeBits);
      }else if(codeType == PANASONIC){
        irsend.sendPanasonic(comando.toInt(), codeBits);
      }else if(codeType == JVC){
        irsend.sendJVC(comando.toInt(), codeBits, 1);
      //}else if(codeType == SANYO){
        //irsend.sendSanyo(comando.toInt(), codeBits);
      //}else if(codeType == MITSUBISHI){
        //irsend.sendMitsubishi(comando.toInt(), codeBits);
      }else if(codeType == UNKNOWN || codeType == SANYO || codeType == MITSUBISHI){
        carregarRawCodes();
        irsend.sendRaw(rawCodes, codeBits, 38);
      }
      stringRead = "";
      irrecv.enableIRIn();
    }
    
    if(stringRead.indexOf("*") != 0){
      stringRead = "";
    }
    
  }
  
  //Receiver
  if (irrecv.decode(&results)) {
    if(results.bits > 0){
      Serial.print("*");
      if(results.decode_type == UNKNOWN || results.decode_type == SANYO || results.decode_type == MITSUBISHI){
        // To store raw codes:
        // Drop first value (gap)
        // Convert from ticks to microseconds
        // Tweak marks shorter, and spaces longer to cancel out IR receiver distortion
        codeBits = results.rawlen - 1;
        for (int i = 1; i <= codeBits; i++) {
          if (i % 2) {
            // Mark
            Serial.print(results.rawbuf[i]*USECPERTICK - MARK_EXCESS, DEC);
          }else {
            // Space
            Serial.print(results.rawbuf[i]*USECPERTICK + MARK_EXCESS, DEC);
          }
          if(i < results.rawlen - 1){
            Serial.print("|");
          }
        }
      }else{
        Serial.print(results.value);
        codeBits = results.bits;
      }
      Serial.print(",");
      Serial.print(results.decode_type);
      Serial.print(",");
      Serial.print(codeBits);
      Serial.print("#");
      Serial.println("");
      
      
    }
    irrecv.resume(); // Receive the next value
  }
}